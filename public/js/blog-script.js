$(document).ready(function() {

	$('a.view-all-posts').click(function() {
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		$('div#default').hide();
		$('div#filter-posts').hide();
		$('div#all-posts').show();
	});

	$('a.filter-posts-by-author').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$('div#all-posts').toggleClass('inactive');
		$('div#filter-posts').toggleClass('active').toggleClass('inactive');
	});

});