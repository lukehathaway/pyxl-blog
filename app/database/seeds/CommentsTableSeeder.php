<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 5) as $index)
		{
			Comment::create([

				'user_id' => rand(1,5),
				'body' => $faker->realText(150),
				'email' => $faker->email(),
				'approved' => 1

			]);
		}
	}

}
