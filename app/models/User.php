<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = array('password', 'remember_token');

	/**
	* The only fields that should be allowed to be mass assigned
	*
	* @var array
	*/

	protected $fillable = array('email', 'password', 'name');

	/**
	* The fields that should not be allowed to be mass assigned
	*
	* @var array
	*/

	protected $guarded = array();

	/**
	* Validation rules
	*
	* @var array
	*/

	public static $rules = array(

		'email' => 'required|email',
		'password' => 'required',
		'name' => 'required'

	);

	/**
	* Authentication rules
	*
	* @var array
	*/

	public static $auth_rules = array(

		'email' => 'required|email',
		'password' => 'required'
	
	);

	/**
	* Post relationship
	*/

	public function posts() {
		return $this->hasMany('Post');
	}

		/**
	* Post relationship
	*/

	public function comments() {
		return $this->hasMany('Comment');
	}

	public function setPasswordAttributes($password) {
		$this->attributes['password'] = Hash::make($password);
	}

}
