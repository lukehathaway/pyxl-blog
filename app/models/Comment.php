<?php

class Comment extends \Eloquent {
	
	protected $fillable = array('body', 'post_id', 'user_id', 'email', 'approved');

	/**
	* Validation rules
	*
	* @var array
	*/
	
	public static $rules = array(
		'body' => 'required',
		'id' => 'integer',
		'post_id' => 'integer',
		'user_id' => 'integer'
	);

	/**
	* Post relationship
	*/

	public function post() {
		return $this->belongsTo('Post', 'post_id');
	}

	public function user() {
		return $this->belongsTo('User', 'user_id');
	}


}