<?php

class Post extends \Eloquent {

	/**
	* Validation rules
	*
	* @var array
	*/
	
	public static $rules = array(
		'title' => 'required|between:3,255',
		'body' => 'required',
		'user_id' => 'integer'
	);

	/**
	* The only fields that should be allowed to be mass assigned
	*
	* @var array
	*/

	protected $fillable = array('title', 'body', 'user_id');

	
	/**
	* User relationship
	*/

	public function user() {
		return $this->belongsTo('User');
	}

	/**
	* Comments relationship
	*/

	public function comments() {
		return $this->hasMany('Comment');
	}

}