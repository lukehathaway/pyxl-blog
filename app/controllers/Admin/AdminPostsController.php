<?php

class AdminPostsController extends \BaseController {

	/**
	 * Display a listing of posts
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = Post::with('user')
						 ->where('posts.user_id', '=', Auth::id())
		         ->get();

		return View::make('admin.posts.index', compact('posts'));
	}

	/**
	 * Show the form for creating a new post
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.posts.create');
	}

	/**
	 * Store a newly created post in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Post::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
			// return Response::json((array('success' => false, 'errors' => $validator, 'message' => 'All fields are required'));
		}

		Post::create($data);
		// return Response::json(array('success' => true, 'errors' => '', 'message' => 'Post created successfully.'));
		return Redirect::route('admin.posts.index');
	}


	/**
	 * Show the form for editing the specified post.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);

		return View::make('admin.posts.edit', compact('post'));
	}

	/**
	 * Update the specified post in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Post::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Post::$rules);

		if ($validator->fails())
		{
			// return Response::json((array('success' => false, 'errors' => $validator, 'message' => 'All fields are required'));
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$post->update($data);

		// return Response::json(array('success' => true, 'errors' => '', 'message' => 'Post created successfully.'));

		return Redirect::route('admin.posts.index');
	}

	/**
	 * Remove the specified post from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Post::destroy($id);

		return Redirect::route('admin.posts.index');
	}

	public function cancel()
	{
		return Redirect::route('admin.posts.index');
	}

}
