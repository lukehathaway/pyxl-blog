<?php

class PostsController extends \BaseController 
{

	public function getIndex() {

		$posts = Post::with('user')->orderBy('created_at', 'desc')->paginate(3);
		
		$posts->getFactory()->setViewName('pagination::simple');

		return View::make('posts.index', compact('posts'));

	}

	public function getPost($id) {

		$post = Post::with('user')->find($id);

    $comment = Comment::with('post', 'user')
                        ->where('post_id', '=', $post->id)
                        ->get();

    $user = Auth::user();
   	
		return View::make('posts.post', compact('post', 'comment', 'user', 'comment_user'));

	}

	public function getPostByAuthor($id) {

		$id = Input::get('user_id');

		$posts = Post::where('user_id', $id)->get();

		$author = User::where('id', $id)->get();

		return View::make('posts.filtered', compact('posts', 'id', 'author'));

	}

	public function json() {

		$today = new DateTime();

		return Post::select('users.name AS Author', 'posts.title AS Title', 'posts.body AS Body', 'posts.created_at AS Posted On')
								 ->join('users', 'posts.user_id', '=', 'users.id')
								 ->where('posts.created_at', '>', $today->modify('-2 days'))
								 ->orderBy('posts.created_at', 'desc')
								 ->get();

	}

}