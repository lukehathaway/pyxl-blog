@extends('admin._layouts.admin')

@section('content')

	<h1>My Posts</h1>
	@if(count($posts))
		<ul id="post-list">
			@foreach($posts as $post)
				<li>
					{{ link_to_route('admin.posts.edit', $post->title, array($post->id)) }}
					{{ Form::open(array('route' => array('admin.posts.destroy', $post->id), 'method' => 'delete', 'class' => 'destroy')) }}
					{{ Form::submit('Delete', array('class' => 'delete')) }}
					{{ Form::close() }}
				</li>
			@endforeach
		</ul>

	@endif

@stop