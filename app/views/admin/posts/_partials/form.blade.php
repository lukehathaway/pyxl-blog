<ul id="post-create">
	{{ Form::hidden('user_id', Auth::user()->id) }}
	<li>
		{{ Form::label('title', 'Title') }}
		{{ Form::text('title') }}
		{{ $errors->first('title', '<p class="error">:message</p>') }}
	</li>
	
	<li>
		{{ Form::label('body', 'Body') }}
		{{ Form::textarea('body') }}
		{{ $errors->first('body', '<p class="error">:message</p>') }}
	</li>

	<li>
		{{ Form::submit('Save') }}
		<a href="/admin/posts" class="button">Cancel</a>
	</li>

</ul>