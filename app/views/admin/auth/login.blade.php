@extends('admin._layouts.admin')

@section('content')
	@if($errors->any())
		<!-- <p>{{ $errors->first() }}</p> -->
	@endif
	<div id="login">
		<h2>Login</h2>
		{{ Form::open(array('admin.login.post')) }}

		<ul>
			<li>
				{{ Form::label('email', 'Email') }}
				{{ Form::email('email') }}
				{{ $errors->first('email', '<p class="error">:message</p>') }}
			</li>
			<li>
				{{ Form::label('password', 'Password') }}
				{{ Form::password('password') }}
				{{ $errors->first('password', '<p class="error">:message</p>') }}
			</li>
			<li>
				{{ Form::submit('Go', array('class' => 'button')) }}
				
			</li>

		</ul>

		{{ Form::close() }}
	</div>
@stop