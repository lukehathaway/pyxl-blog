@extends('_layouts.default')

@section('content')
	<div id="filtered-results">
		@foreach($author as $author)
			<span>All posts by <strong>{{{ $author->name }}}</strong></span>
		@endforeach

		<ul id="filtered-posts-list">
		@foreach($posts as $post)
			<li>
				{{ link_to_route('post', $post->title, array($post->id)) }}
			</li>
		@endforeach
		</ul>
	</div>
 	<p>Not what you were looking for? Try a different author:</p>
	<div id="filter-posts" class="">

		@include('posts.filter')
	
	</div>

@stop