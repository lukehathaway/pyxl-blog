@extends('_layouts.default')

@section('content')

	<h2>{{{ $post->title }}}</h2>
	<p class="created_at">Created on {{{ date('m-d-Y', strtotime($post->created_at)) }}} By {{{ $post->user->name }}}</p>
	<p class="post-body">{{{ $post->body }}}</p>
	<p>{{ link_to_route('home', '&lsaquo; Back') }}</p>
	
	<div class="comments">
		<h2>Comments</h2>
		@if (!Auth::check())
			<p class="note"><em>You must {{ link_to_route('admin.login', 'login') }} to add comments.</em></p>
		@endif
		@include('comments.view')

		@if (Auth::check())
			@include('comments.create')
		@endif
		
	</div>
	
@stop