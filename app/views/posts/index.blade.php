@extends('_layouts.default')

@section('content')
	
	<div id="default">
		
		<p>Welcome</p>
		<p>This is Dempsey. If you're wondering where the name came from, there are a few reasons for it. First, the name Dempsey just sounds cool. Secondly, Clinton Drew Dempsey is an American hero. Finally, who wants to have a blogging platform named "NewCo"?</p>
		<p>Enough about me. Login, sign up, or just browse my stories below.</p>

	</div>

	<div id="view-choice">

		<a href="#" class="filter-posts-by-author button">filter by author</a>
	
	</div>
	
	
	<div id="filter-posts" class="inactive">

		@include('posts.filter')
	
	</div>

	<div id="all-posts">

		@if(count($posts) > 0)

			@foreach($posts as $post)
				<article>		
					<h2>{{ link_to_route('post', $post->title, array($post->id)) }}</h2>
					<p class="created_at">Created on {{{ date('m-d-Y', strtotime($post->created_at)) }}} By {{{ $post->user->name }}}</p>
					<p>{{{ str_limit($post->body, 200, '...') }}}</p>
					<p class="read-more button">{{ link_to_route('post', 'Read more &rsaquo;', array($post->id)) }}</p>
				</article>
			
			@endforeach

		@else
			<em>No posts to share...it feels awfully lonely around here</em>
		@endif

		{{ $posts->links(); }}
	
	</div>

@stop

