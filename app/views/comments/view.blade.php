@if(count($comment) > 0)
	<ul id="comments-list">
		@foreach($comment as $comm)
			@if($comm->user)
				<li>
					<p class="comment-body">{{{ $comm->body }}}</p>
					<p> - {{{ $comm->user->name }}}</p>
					@if (Auth::check() && $comm->user->id == Auth::id())
						{{ Form::open(array('route' => array('comments.destroy', $comm->id), 'method' => 'delete', 'class' => 'destroy')) }}
						{{ Form::submit('Delete', array('class' => 'delete')) }}
						{{ Form::close() }}
					@endif
				</li>
			@endif

		@endforeach
		
	</ul>
@else 
	<p><em>There are not any comments on this post yet. Start the conversation!</em></p> 
@endif
