<h2>Add a comment</h2>	

{{ Form::open(array('route' => 'comments.store')) }}
	<ul id="create-comment">
		<li>
			{{ Form::textarea('body') }}
			{{ $errors->first('body', '<p class="error">:message</p>') }}
		</li>
			{{ Form::hidden('post_id', $post->id) }}
			{{ Form::hidden('user_id', Auth::id()) }}
			{{ Form::hidden('email', $user->email) }}
			{{ Form::hidden('approved', 0) }}
		<li>
			{{ Form::submit('Save') }}
		</li>
	</ul>
	
{{ Form::close() }}