@extends('_layouts.default')

@section('content')
	<ul>
		@foreach($comments as $comment)
			<li> 
				CommentID: {{ $comment->post_id }}
				{{ $comment->body }} 
			</li>
		@endforeach
	</ul>
@stop