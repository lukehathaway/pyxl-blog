<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Blog</title>
	
	{{ HTML::style('css/blog-style.css') }}
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

</head>
<body>
	
	<header>
		<div id="masthead">
			{{ link_to_route('home', 'Dempsey') }}
		</div>
		@if(!Auth::check())
			<div class="not-logged-in">
				<div class="button">
					{{ link_to_route('admin.login', 'Login') }}
				</div>
				<div class="button">
					{{ link_to_route('user.create', 'Sign Up') }}
				</div>
			</div>
		@else
			<div class="logged-in">
				<p>Logged in as: {{ Auth::user()->name }}</p>
				<div class="button">
					{{ link_to_route('admin.posts.create', 'Create Post') }}
				</div>
				<div class="button">
				 	@include('admin.auth.logout')
				</div>
			</div>
		@endif
	</header>
	<div class="messages">
		@if(Session::has('message'))
			<p class="alert">{{ Session::get('message') }}</p>
		@endif
	</div>
	<main>
		@yield('content')
	</main>

	<footer>
		<div class="container">
			&copy; {{ date('Y') }} Luke Hathaway | {{ link_to_route('admin.posts.index', 'Admin') }}
		</div>
	</footer>
	{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') }}
	{{ HTML::script('js/blog-script.js') }}
</body>
</html>