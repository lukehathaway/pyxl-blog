@extends('admin._layouts.admin')

@section('content')

	<div id="register">
		<h2>Register</h2>

		{{ Form::open(array('route' => array('user.store'), 'method' => 'post')) }}
			<ul>
				<li>
					{{ Form::label('name', 'Name') }}
					{{ Form::text('name') }}
				</li>

				<li>
					{{ Form::label('email', 'Email Address') }}
					{{ Form::email('email') }}
				</li>
				<li>
					{{ Form::label('password', 'Password') }}
					{{ Form::password('password') }}
				</li>
				<li>
					{{ Form::submit('Go', array('class' => 'button')) }}
				</li>
			</ul>

		{{ Form::close() }}
	</div>


@stop