<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'PostsController@getIndex'));


Route::group(array('prefix' => 'admin'), function() {

	Route::get('login', array('as' => 'admin.login', 'uses' => 'AdminAuthController@getLogin'));
	Route::post('login', array('as' => 'admin.login.post', 'uses' => 'AdminAuthController@postLogin'));
	Route::get('logout', array('as' => 'admin.logout', 'uses' => 'AdminAuthController@getLogout'));

});

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function() {

	Route::resource('posts', 'AdminPostsController', array('except' => array('show')));

});

Route::post('filter/author/{id}', array('as' => 'filter.author', 'uses' => 'PostsController@getPostByAuthor'));
Route::get('post/{id}', array('as' => 'post', 'uses' => 'PostsController@getPost'))->where('id', '[1-9][0-9]*');

Route::post('/post/{post}/comment', array('as' => 'comment.create', 'uses' => 'CommentController@create'));

Route::resource('user', 'UsersController');
Route::resource('comments', 'CommentController', array('except' => array('show')));

Route::group(array('prefix' => 'api/v1'), function() {

	

});

Route::resource('feed', 'PostsController@json');
